package com.everis.school.productsms.mapper;

import java.util.List;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import com.everis.school.productsms.dto.ProductDto;
import com.everis.school.productsms.dto.ProductSaveRequestDto;
import com.everis.school.productsms.dto.ProductSaveResponseDto;
import com.everis.school.productsms.entity.Product;

@Mapper
public interface ProductMapper {
	
	ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class); 
	
	public ProductDto map(Product produt);
	public List<ProductDto> map(List<Product> products);
	public Product toEnitity(ProductSaveRequestDto productSaveRequestDto);
	public ProductSaveResponseDto productSaveResponseDto(Product product);

	@AfterMapping
	default void setRemainingValues(Product product, @MappingTarget ProductDto productDto) {
		productDto.setStatus(Boolean.TRUE.equals(product.getActive()) ? "Active" : "Inactivo");
		
	}
}
