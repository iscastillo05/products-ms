package com.everis.school.productsms.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDto {
	private Long idProduct;
	private String name;
	private String description;
	private Double price;
	private String status;
}
