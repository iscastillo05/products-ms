//package com.everis.school.productsms.controller;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.everis.school.productsms.dto.ProductDto;
//import com.everis.school.productsms.dto.ProductSaveRequestDto;
//import com.everis.school.productsms.dto.ProductSaveResponseDto;
//import com.everis.school.productsms.entity.Product;
//import com.everis.school.productsms.service.ProductService;
//
//@RestController
//public class ProductController {
//
//	@Autowired
//	private ProductService productService;
//
//	@GetMapping("/products")
//	public List<ProductDto> list() {
//		List<Product> productList = productService.listAll();
//		List<ProductDto> productListDto = new ArrayList<>();
//		productList.forEach(product -> {
//			ProductDto productDto = convertToProductDto(product);
//			productListDto.add(productDto);
//		});
//		return productListDto;
//	}
//
//	@GetMapping("/findById/{id}")
//	public ProductDto findById(@PathVariable("id") Long idProduct) {
//		Product product = productService.findById(idProduct);
//		return convertToProductDto(product);
//
//	}
//
//	@PostMapping("product/save")
//	public ProductSaveResponseDto save(@RequestBody ProductSaveRequestDto productSaveRequestDto) {
//		Product product = new Product();
//		product.setName(productSaveRequestDto.getName());
//		product.setDescription(productSaveRequestDto.getDescription());
//		product.setPrice(productSaveRequestDto.getPrice());
//
//		Product productSave = productService.save(product);
//
//		ProductSaveResponseDto productSaveResponseDto = new ProductSaveResponseDto();
//		productSaveResponseDto.setIdProduct(productSave.getIdProduc());
//		productSaveResponseDto.setName(productSave.getName());
//		productSaveResponseDto.setIdProduct(productSave.getIdProduc());
//		productSaveResponseDto.setIdProduct(productSave.getIdProduc());
//		return productSaveResponseDto;
//	}
//
//	private ProductDto convertToProductDto(Product product) {
//		ProductDto productDto = new ProductDto();
//		productDto.setIdProduct(product.getIdProduc());
//		productDto.setName(product.getName());
//		productDto.setDescription(product.getDescription());
//		productDto.setPrice(product.getPrice());
//		productDto.setStatus(Boolean.TRUE.equals(product.getActive()) ? "Active" : "Inactivo");
//		return productDto;
//	}
//
//}
