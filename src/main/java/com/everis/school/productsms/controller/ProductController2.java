package com.everis.school.productsms.controller;

//import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.school.productsms.dto.ProductDto;
import com.everis.school.productsms.dto.ProductSaveRequestDto;
import com.everis.school.productsms.dto.ProductSaveResponseDto;
import com.everis.school.productsms.entity.Product;
import com.everis.school.productsms.mapper.ProductMapper;
import com.everis.school.productsms.service.ProductService;

@RestController
public class ProductController2 {

	@Autowired
	private ProductService productService;
	
//	@Autowired
//	private ProductMapper productMapper;
	

	@GetMapping("/products")
	public List<ProductDto> list() {
		return ProductMapper.INSTANCE.map(productService.listAll());
		
	}

	@GetMapping("/findById/{id}")
	public ProductDto findById(@PathVariable("id") Long idProduct) {
		return ProductMapper.INSTANCE.map(productService.findById(idProduct));
	}

	@PostMapping("product/save")
	public ProductSaveResponseDto save(@RequestBody ProductSaveRequestDto productSaveRequestDto) {
		

		Product productSave = productService.save(ProductMapper.INSTANCE.toEnitity(productSaveRequestDto));

		return ProductMapper.INSTANCE.productSaveResponseDto(productSave);
	}

}
