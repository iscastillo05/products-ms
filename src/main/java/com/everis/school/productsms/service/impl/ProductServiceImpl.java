package com.everis.school.productsms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.everis.school.productsms.entity.Product;
import com.everis.school.productsms.repository.ProductRepository;
import com.everis.school.productsms.service.ProductService;

@Service
//@Component
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public List<Product> listAll() {
		return productRepository.findAll();
	}

	@Override
	public Product findById(Long idProduct) {
		return productRepository.findById(idProduct).orElse(null);
	}

	@Override
	public Product save(Product product) {
		product.setActive(Boolean.TRUE);
		return productRepository.save(product);
	}

}
