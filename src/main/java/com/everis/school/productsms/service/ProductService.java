package com.everis.school.productsms.service;

import java.util.List;

import com.everis.school.productsms.entity.Product;

public interface ProductService {

	public List<Product> listAll();
	public Product findById(Long idProduct);
	public Product save(Product product);
	
}
